package com.jeesuite.gateway;

public class FilterConstants {

	public static final String CONTEXT_ROUTE_SERVICE = "ctx-route-svc";
	public static final String CONTEXT_IGNORE_FILTER = "ctx-ignore-filter";
	public static final String CONTEXT_TRUSTED_REQUEST = "ctx-trusted-req";
	
	public static final String X_SIGN_HEADER = "x-open-sign";
	public static final String APP_ID_HEADER = "x-open-appId";
	public static final String TIMESTAMP_HEADER = "timestamp";
}
