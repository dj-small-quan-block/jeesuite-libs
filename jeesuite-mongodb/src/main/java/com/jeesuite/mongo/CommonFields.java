package com.jeesuite.mongo;

/**
 * 
 * <br>
 * Class Name   : CommonFields
 *
 * @author jiangwei
 * @version 1.0.0
 * @date 2020年8月20日
 */
public class CommonFields {

	public static final String ID_FIELD_NAME = "_id";
	public static final String ID_FIELD_NAME_ALIAS = "id";
	public static final String SHOP_ID_FIELD_NAME = "shopId";
	public static final String UPDATE_BY_FIELD_NAME = "updatedBy";
	public static final String UPDATE_AT_FIELD_NAME = "updatedAt";
	public static final String CREATE_BY_FIELD_NAME = "createdBy";
	public static final String CREATE_AT_FIELD_NAME = "createdAt";
	public static final String ENABLED_FIELD_NAME = "enabled";
	public static final String DELETED_FIELD_NAME = "deleted";
}
